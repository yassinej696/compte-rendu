package com.yassine.tp.Repository;

import com.yassine.tp.Model.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductModel, Integer> {


}
