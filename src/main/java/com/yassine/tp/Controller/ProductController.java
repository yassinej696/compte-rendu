package com.yassine.tp.Controller;

import com.yassine.tp.Model.ProductModel;
import com.yassine.tp.Services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
@Autowired
    ProductService productService;
  @GetMapping("/affiche")
  public List<ProductModel> FindAllProduct(){
      return this.productService.FindAllProduct();
  }
  @PostMapping("/add")
  public ProductModel AjoutProduct(@RequestBody ProductModel pr){
      return this.productService.AjoutProduct(pr);
  }
}
