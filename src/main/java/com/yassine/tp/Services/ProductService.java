package com.yassine.tp.Services;

import com.yassine.tp.Model.ProductModel;
import com.yassine.tp.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public ProductModel AjoutProduct(ProductModel produit){
        return this.productRepository.save(produit);
    }
    public List<ProductModel> FindAllProduct(){
        return this.productRepository.findAll() ;
    }
    public ProductModel UpDateProduct(ProductModel product){
        return this.productRepository.save(product);

    }
    public long CountProduct(){
        return this.productRepository.count();
    }
    public void Delete(int idProduct){
         this.productRepository.deleteById(idProduct);
    }
}
